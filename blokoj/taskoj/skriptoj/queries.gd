extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "Задачи"


# количество объектов для загрузки
const count_objekto = "5"


func taskoj_projekto_ws(id, after):
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'query ($realecoId:Float ) { '+
		'universoProjekto (realeco_Id:$realecoId, '+
		" first:"+count_objekto+', after: "'+after+'" ' +
		" ) { pageInfo { hasNextPage endCursor } edges { node { "+
		' uuid nomo { enhavo } priskribo { enhavo }'+
		' } } } }',
		"variables": {"realecoId":Global.realeco} }})
	# print('===taskoj_projekto_ws==',query)
	return query


func taskoj_query_ws(id, after):
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'query ($realecoId:Float ) { '+
		'universoTasko (realeco_Id:$realecoId '+
		" first:"+count_objekto+', after: "'+after+'" ' +
		" ) { pageInfo { hasNextPage endCursor } edges { node { "+
		' uuid nomo { enhavo } priskribo { enhavo } '+
		' } } } }',
		"variables": {"realecoId":Global.realeco} }})
	# print('=== taskoj_query_ws ==',query)
	return query
