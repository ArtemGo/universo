extends ItemList


# Обработчик выбора элемент списка ItemList
func _on_ItemList_item_selected(index):
	var SelectedText = $"../".ItemListContent[index]
	# Назначаем текст виджету DetailLabel
	$"../".get_node("DetailLabel").set_text(SelectedText)

