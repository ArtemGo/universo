#disco
extends TextureButton

#при наведении стрелки мыши выключает отображение disco
#включает отображение pauzo
func _on_disco_mouse_entered():
	get_node("/root/Title/gramofono/regpanelo/fono/disco").set_visible(false)
	get_node("/root/Title/gramofono/regpanelo/fono/pauzo").set_visible(true)
