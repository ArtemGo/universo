extends HSlider

#регулировка громкости
func _on_reguligo_lauteco_value_changed(value):
	AudioServer.set_bus_volume_db(0, value)

#volumeno скрывается, когда мышь покидает область reguligo_lauteco
func _on_reguligo_lauteco_mouse_exited():
	get_node("/root/Title/gramofono/volumeno").set_visible(false)
